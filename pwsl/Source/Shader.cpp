#include <pwsl/Shader.hpp>

using namespace pwsl;
Shader::Shader(const GLchar* vertex, const GLchar* fragment)
{
	// Retrieve vertex and fragment source code from a file path.
	std::ifstream vertex_file;
	std::ifstream fragment_file;
	std::stringstream vertex_stream;
	std::stringstream fragment_stream;

	vertex_file.open(vertex);
	fragment_file.open(fragment);

	vertex_stream << vertex_file.rdbuf();
	fragment_stream << fragment_file.rdbuf();

	vertex_file.close();
	fragment_file.close();


	// Shader compile status.
	GLint compile_status = GL_FALSE;

	// Vertex shader creation.
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	std::string vertex_shader_string = vertex_stream.str();
	const char * vertex_shader_code = vertex_shader_string.c_str();

	glShaderSource(vertex_shader, 1, (const GLchar **)&vertex_shader_code,
		NULL);
	glCompileShader(vertex_shader);
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &compile_status);

	if (compile_status != GL_TRUE)
	{
		const int string_length = 1024;
		GLchar compile_log[string_length] = "";
		glGetShaderInfoLog(vertex_shader, string_length, NULL, compile_log);
		std::cerr << compile_log << std::endl;
	}

	// Fragment shader creation.
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	std::string fragment_shader_string = fragment_stream.str();
	const char * fragment_shader_code = fragment_shader_string.c_str();

	glShaderSource(fragment_shader, 1, (const GLchar **)&fragment_shader_code,
		NULL);
	glCompileShader(fragment_shader);
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compile_status);

	if (compile_status != GL_TRUE)
	{
		const int string_length = 1024;
		GLchar compile_log[string_length] = "";
		glGetShaderInfoLog(fragment_shader, string_length, NULL, compile_log);
		std::cerr << compile_log << std::endl;
	}

	// Shader program generation.
	ID = glCreateProgram();
	glAttachShader(ID, vertex_shader);
	glBindAttribLocation(ID, 0, "vertex_position");
	glBindAttribLocation(ID, 1, "vertex_normal");
	glDeleteShader(vertex_shader);

	glAttachShader(ID, fragment_shader);
	glDeleteShader(fragment_shader);
	glLinkProgram(ID);

	GLint link_status = 0;
	glGetProgramiv(ID, GL_LINK_STATUS, &link_status);
	if (link_status != GL_TRUE)
	{
		const int string_length = 1024;
		GLchar link_log[string_length] = "";
		glGetProgramInfoLog(ID, string_length, NULL, link_log);
		std::cerr << link_log << std::endl;
	}
}

Shader::~Shader()
{
}

// Activate shader program.
void Shader::UseShader()
{
	glUseProgram(ID);
}

// Utility functions to set different variable types:
// Boolean, Integer, Float, Vectors and Matrices.

void Shader::SetBoolean(const std::string &id, bool value) const
{
	glUniform1i(glGetUniformLocation(ID, id.c_str()), (int)value);
}

void Shader::SetInteger(const std::string &id, int value) const
{
	glUniform1i(glGetUniformLocation(ID, id.c_str()), value);
}

void Shader::SetFloat(const std::string &id, float value) const
{
	glUniform1f(glGetUniformLocation(ID, id.c_str()), value);
}

void Shader::SetVec2(const std::string & id, const glm::vec2 & value) const
{
	glUniform2fv(glGetUniformLocation(ID, id.c_str()), 1, &value[0]);
}

void Shader::SetVec2(const std::string & id, float x, float y) const
{
	glUniform2f(glGetUniformLocation(ID, id.c_str()), x, y);
}

void Shader::SetVec3(const std::string & id, const glm::vec3 & value) const
{
	glUniform3fv(glGetUniformLocation(ID, id.c_str()), 1, &value[0]);
}

void Shader::SetVec3(const std::string & id, float x, float y, float z) const
{
	glUniform3f(glGetUniformLocation(ID, id.c_str()), x, y, z);
}

void Shader::SetVec4(const std::string & id, const glm::vec4 & value) const
{
	glUniform4fv(glGetUniformLocation(ID, id.c_str()), 1, &value[0]);
}

void Shader::SetVec4(const std::string & id, float x, float y, float z, float w) const
{
	glUniform4f(glGetUniformLocation(ID, id.c_str()), x, y, z, w);
}

void Shader::SetMatrix2(const std::string & id, const glm::mat2 & mat) const
{
	glUniformMatrix2fv(glGetUniformLocation(ID, id.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void Shader::SetMatrix3(const std::string & id, const glm::mat3 & mat) const
{
	glUniformMatrix3fv(glGetUniformLocation(ID, id.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void Shader::SetMatrix4(const std::string & id, const glm::mat4 & mat) const
{
	glUniformMatrix4fv(glGetUniformLocation(ID, id.c_str()), 1, GL_FALSE, &mat[0][0]);
}
