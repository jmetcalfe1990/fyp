#version 420 core

uniform vec3 resolution;
uniform float wave_time;

const float PI         = 3.1415;
//const float EPSILON    = 1e-3;
//float EPSILON_NRM    = 0.1 / 1280;

// sea
const float SEA_HEIGHT = 0.6;
const float SEA_SPEED = 0.8;
const vec3 SEA_BASE = vec3(0.1,0.19,0.22);
const vec3 SEA_WATER_COLOUR = vec3(0.8,0.9,0.6);

// lighting
float diffuse(vec3 n,vec3 l,float p) 
{
	return pow(dot(n,l) * 0.4 + 0.6,p);
}

float specular(vec3 n,vec3 l,vec3 e,float s) 
{    
	float nrm = (s + 8.0) / (PI * 8.0);
	return pow(max(dot(reflect(e,n),l),0.0),s) * nrm;
}

// sky
vec3 getSkyColour(vec3 e) 
{
	e.y = max(e.y,0.0);
	vec3 ret;
	ret.x = pow(1.0-e.y,2.0);
	ret.y = 1.0-e.y;
	ret.z = 0.6+(1.0-e.y)*0.4;
	return ret;
}

// sea
vec3 getSeaColour(vec3 p, vec3 n, vec3 l, vec3 eye, vec3 dist) 
{  
	float fresnel = 1.0 - max(dot(n,-eye),0.0);
	fresnel = pow(fresnel,3.0) * 0.65;
	    
	vec3 reflected = getSkyColour(reflect(eye,n));    
	vec3 refracted = SEA_BASE + diffuse(n,l,80.0) * SEA_WATER_COLOUR * 0.12; 
	
	vec3 colour = mix(refracted,reflected,fresnel);
	
	float atten = max(1.0 - dot(dist,dist) * 0.001, 0.0);
	colour += SEA_WATER_COLOUR * (p.y - SEA_HEIGHT) * 0.18 * atten;
	
	colour += vec3(specular(n,l,eye,60.0));
	
	return colour;
}

in vec3 direction;
in vec3 normal;
in vec3 p;
in vec3 distance;
//in vec2 fragCoord;

out vec4 fragment_colour;

// main
void main(void) 
{
	// tracing
	vec3 light = normalize(vec3(0.0,1.0,0.8)); 
	             
	// colour
	vec3 colour = mix(getSkyColour(direction),
										getSeaColour(p, normal, light, direction, distance),
										pow(smoothstep(0.0, -0.05, direction.y), 0.3));
	    
	// post
	fragment_colour = vec4(pow(colour,vec3(0.75)), 1.0);
}