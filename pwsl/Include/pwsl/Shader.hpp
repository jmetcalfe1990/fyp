#pragma once

#include <glad/glad.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <unordered_map>
#include <chrono>

namespace pwsl {
	class Shader
	{
	public:
		Shader(const GLchar* vertex, const GLchar* fragment);
		~Shader();

		GLuint ID{ 0 };

		// Activate shader program.
		void UseShader();

		// Utility functions to set different variable types:
		// Boolean, Integer, Float, Vectors and Matrices.
		void SetBoolean(const std::string &id, bool value) const;
		void SetInteger(const std::string &id, int value) const;
		void SetFloat(const std::string &id, float value) const;

		void SetVec2(const std::string &id, const glm::vec2 & value) const;
		void SetVec2(const std::string &id, float x, float y) const;
		void SetVec3(const std::string &id, const glm::vec3 & value) const;
		void SetVec3(const std::string &id, float x, float y, float z) const;
		void SetVec4(const std::string &id, const glm::vec4 & value) const;
		void SetVec4(const std::string &id, float x, float y, float z, float w) const;

		void SetMatrix2(const std::string &id, const glm::mat2 &mat) const;
		void SetMatrix3(const std::string &id, const glm::mat3 &mat) const;
		void SetMatrix4(const std::string &id, const glm::mat4 &mat) const;
	};
}


