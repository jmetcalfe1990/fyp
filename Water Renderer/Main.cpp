#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Source/Camera.hpp"
#include <vector>
#include <unordered_map>
#include <iostream>
#include <string>
#include <fstream>
#include <time.h>

#include <pwgl/Mesh.hpp>
#include <pwgl/Water.hpp>
#include <pwgl/Texture.hpp>
#include <pwsl/Shader.hpp>

// References

// Glad
// https://github.com/Dav1dde/glad/blob/master/LICENSE

// GLFW
// https://www.glfw.org/license.html

// LearnOpgenGL licensing
// https://creativecommons.org/licenses/by/4.0/

// Camera Creation
// https://learnopengl.com/Getting-started/Camera

// Shader Support and Creation
// https://learnopengl.com/Getting-started/Shaders

// Window Creation
// https://learnopengl.com/Getting-started/Creating-a-window

// LibNoise library
// http://libnoise.sourceforge.net/


using namespace std;

void FramebufferSizeCallBack(GLFWwindow* window, int width, int height);
void ProcessInput(GLFWwindow *window);
void MouseCallBack(GLFWwindow* window, double xPos, double yPos);
void ScrollCallBack(GLFWwindow* window, double xOffset, double yOffset);
std::string StringFromFile(const std::string &filename);

const unsigned int screen_width = 1280;
const unsigned int screen_height = 720;

Camera camera(glm::vec3(0.0f, 1.0f, 3.0f));
float lastX = screen_width / 2.0f;
float lastY = screen_height / 2.0f;
bool mouseEnable = true;

float delta_time = 0.0f;
float last_frame = 0.0f;

int main()
{
	// Initialise and configure GLFW.
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// GLFW window creation.
	GLFWwindow* window = glfwCreateWindow(screen_width, screen_height, "P4112379 James Metcalfe Computing Project", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create a GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, FramebufferSizeCallBack);
	glfwSetCursorPosCallback(window, MouseCallBack);
	glfwSetScrollCallback(window, ScrollCallBack);

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// GLAD: Load all OpenGL function pointers.
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialise GLAD" << std::endl;
		return -1;
	}

	//////////////////// MESH ///////////////////////

	pwgl::Mesh mesh_;

	glViewport(0, 0, screen_width, screen_height);
	glEnable(GL_DEPTH_TEST);

	//////////////////// SHADER /////////////////////

	pwsl::Shader shader_("water_vs.glsl", "water_fs.glsl");
	clock_t start_timer;
	start_timer = clock();

	// Render loop.
	while (!glfwWindowShouldClose(window))
	{
		float current_frame = glfwGetTime();
		delta_time = current_frame - last_frame;
		last_frame = current_frame;

		float modelScale = 0.5f;

		// Input.
		ProcessInput(window);

		// Clear colour and buffers.
		glClearColor(0.f, 0.f, 0.25f, 0.f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Activate shader.
		shader_.UseShader();

		clock_t end_timer;
		end_timer = clock();

		float wave_time((float)end_timer - (float)start_timer);

		float wave_seconds = glfwGetTime();
		
		// Simulate mesh vertices.
		mesh_.Simulate(0.13f);

		//Pass projection matrix to shader.
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)screen_width / (float)screen_height, 0.1f, 100.f);
		shader_.SetMatrix4("projection", projection);

		//Pass camera / view matrix to shader.
		glm::mat4 view = camera.GetViewMatrix();
		shader_.SetMatrix4("view", view);

		//Pass model matrix to shader.
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::scale(model, glm::vec3(modelScale));
		shader_.SetMatrix4("model", model);

		// Render mesh elements and vertices.
		mesh_.BindVAO();
		mesh_.DrawElements();

		// Swap GLFW buffer and poll events such as a key press or mouse movement.
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}

//////////////////// UTILITY ////////////////////

// Callback is initiated when window size is changed.
void FramebufferSizeCallBack(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

// Processes all keyboard input.
// GLFW queries key presses and key release within current frame.
void ProcessInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, true);
	}

	float camera_speed = 5 * delta_time;
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		camera.ProcessKeyboard(FORWARD, delta_time);
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		camera.ProcessKeyboard(BACKWARD, delta_time);
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		camera.ProcessKeyboard(LEFT, delta_time);
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		camera.ProcessKeyboard(RIGHT, delta_time);
	}

	if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
}

// Callback is initiated when mouse pointer is moved.
void MouseCallBack(GLFWwindow * window, double xPos, double yPos)
{
	if (mouseEnable)
	{
		lastX = xPos;
		lastY = yPos;
		mouseEnable = false;
	}

	float xOffset = xPos - lastX;
	float yOffset = lastY - yPos;

	lastX = xPos;
	lastY = yPos;

	camera.ProcessMouseMovement(xOffset, yOffset);
}

// Callback is initiated when mouse scroll wheel is scrolled.
void ScrollCallBack(GLFWwindow* window, double xOffset, double yOffset)
{
	camera.ProcessMouseScrollWheel(yOffset);
}

// Loads a file from a string using ifstream.
std::string StringFromFile(const std::string &filename)
{
	std::ifstream if_str(filename);
	std::string content((std::istreambuf_iterator<char>(if_str)),
		(std::istreambuf_iterator<char>()));

	return content;
}