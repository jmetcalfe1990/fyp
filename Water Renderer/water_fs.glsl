#version 420 core

uniform float time_seconds;

in vec3 fragment_normal;

out vec4 fragment_colour;

void main(void)
{
	vec3 norm = normalize(fragment_normal);
	fragment_colour = vec4(norm.x, 0.25, 0.5, 1.0);
}
