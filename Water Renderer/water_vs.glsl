#version 420 core

uniform float time_seconds;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;

out vec3 fragment_normal;

void main(void)
{
	fragment_normal = vertex_normal;
	gl_Position = projection * view * model * vec4(vertex_position, 1.0);
}