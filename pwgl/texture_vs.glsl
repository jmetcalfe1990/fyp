#version 420 core
layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;
layout(location = 2) in vec2 vertex_texcoord;


out vec3 varying_position;
out vec3 varying_normal;
out vec2 varying_texcoord;

void main(void)
{
	varying_texcoord = vertex_texcoord;
	varying_normal = vertex_normal;
	gl_Position = vec4(vertex_position, 1.0);
}