#pragma once
#include <glad/glad.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <unordered_map>
#include <chrono>

namespace pwgl
{
	class Texture
	{
	public:
		Texture();
		~Texture();

		GLuint water_tex;
		GLuint texture_prog;

		void GenTexture();
		void DeleteTexture();
		void RenderTexture();
		void BindTexture();
	};
}