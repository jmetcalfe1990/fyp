#pragma once

#include <glad/glad.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <chrono>

#include <libnoise/include/noise/noise.h>
#include <noiseutils/noiseutils.h>

namespace pwgl
{
class Water
	{
	public:
		Water();

		// Functionality to generate normals.
		glm::vec3 GetNormals(glm::vec3 position, float eps);

		// Functionality to generate colour for water.
		glm::vec3 GetColour(glm::vec3 p, glm::vec3 n, glm::vec3 l, glm::vec3 eye, glm::vec3 dist);

		// Functionality to generate noise.
		float Noise(glm::vec2 n);

	private:
		const int step_number = 8;
		const float PI = 3.141592;

		const int water_geometry = 3;
		const int water_fragment = 5;

		// Water variables.
		const float wave_height = 0.5f;
		const float wave_speed = 0.5f;
		const float water_frequency = 0.15f;
		const float water_chop = 3.0f;

		float playback_time = 0.0;
		float water_time = 1.0 + playback_time * wave_speed;

		const glm::vec3 water_base = glm::vec3(0.1, 0.19, 0.22);
		const glm::vec3 water_colour = glm::vec3(0.8, 0.9, 0.6);
		glm::mat2 octave = glm::mat2(1.6f, 1.2f, -1.2f, 1.6f);

		// Water manipulation functionality.
		float Hash(glm::vec2 h);
		float WaterOctave(glm::vec2 uv, float chop);
		float WaterMap(glm::vec3 p);
		float WaterMapDetail(glm::vec3 p);

		//Generates a height map for water simulation.
		float HeightMapTrace(glm::vec3 orientation, glm::vec3 direction, glm::vec3 pos);

		// Generate euler matrices.
		glm::mat3 GetEulerMatrices(glm::vec3 angle);
	};
}