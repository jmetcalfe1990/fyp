#pragma once

#include <glad/glad.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <memory>

extern "C"
{
#include "Config.hpp"
}
#include <libnoise/include/noise/noise.h>
#include <noiseutils/noiseutils.h>

struct Vertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 texCoord;
	glm::vec3 tangent;
	glm::vec3 bitangent;
};

struct Texture
{
	unsigned int id;
	std::string type;
	std::string path;
};

namespace pwgl {
	class Mesh
	{
	public:
		Mesh();
		~Mesh();

		int GetVertices();
		unsigned int GetElements();
		GLuint GetVAO();
		void BindVAO();
		void DrawElements();
		void Simulate(float deltaTime);

	private:
		std::vector<Vertex> vertices;
		std::vector<unsigned int> elements;
		std::vector<Texture> textures;

		unsigned int cell_width = 64;
		unsigned int cell_height = 64;
		unsigned int vertex_width = cell_width + 1;
		unsigned int vertex_height = cell_height + 1;

		float cell_size = 1.0f;
		float time = 0;
		GLuint vertex_vbo{ 0 };
		GLuint element_vbo{ 0 };
		GLuint vao{ 0 };

		// Noise data provided by libnoise library to simulate noise and create a height map.
		noise::module::Perlin water_noise;
		utils::NoiseMap water_heightmap;
		utils::NoiseMapBuilderPlane water_mapbuilder;
		utils::RendererImage water_renderer;
		utils::Image water_image;
		utils::WriterBMP image_writer;

		void ApplyNoise();
		void CreateMesh();
		void DeleteMesh();
		void BuildMesh(); 
		void PopulateVerts();
		void PopulateNormals();
		void UpdateBuffer();
	};
}
