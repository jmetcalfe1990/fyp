#include <pwgl/Water.hpp>
#include <pwgl/Mesh.hpp>

using namespace pwgl;

Water::Water()
{
}

// Functionality to generate normals.
glm::vec3 Water::GetNormals(glm::vec3 position, float eps)
{
	glm::vec3 norm;
	norm.y = WaterMapDetail(position);
	norm.x = WaterMapDetail(glm::vec3(position.x + eps, position.y, position.z)) - norm.y;
	norm.z = WaterMapDetail(glm::vec3(position.x, position.y, position.z + eps)) - norm.y;
	norm.y = eps;

	return glm::normalize(norm);
}

// Functionality to generate colour for water.
glm::vec3 Water::GetColour(glm::vec3 p, glm::vec3 n, glm::vec3 l, glm::vec3 eye, glm::vec3 dist)
{
	return glm::vec3();
}

// Water manipulation functionality.
float Water::Hash(glm::vec2 h)
{
	float hash = glm::dot(h, glm::vec2(127.1, 311.7));
	return glm::fract(sin(hash) * 43758.5453123);
}

// Functionality to generate noise.
float Water::Noise(glm::vec2 n)
{
	glm::vec2 i = glm::floor(n);
	glm::vec2 f = glm::fract(n);
	glm::vec2 u = f * f * (3.0f - 2.0f * f);

	return -1.0 + 2.0 * glm::mix(glm::mix(Hash(i + glm::vec2(0.0, 0.0)),
		Hash(i + glm::vec2(1.0, 0.0)), u.x),
		glm::mix(Hash(i + glm::vec2(0.0, 1.0)),
			Hash(i + glm::vec2(1.0, 1.0)), u.x), u.y);
}

float Water::WaterOctave(glm::vec2 uv, float chop)
{
	uv += Noise(uv);
	glm::vec2 wave = 1.0f - glm::abs(sin(uv));
	glm::vec2 s_wave = glm::abs(cos(uv));

	wave = glm::mix(wave, s_wave, wave);

	return glm::pow(1.0 - glm::pow(wave.x * wave.y, 0.65), chop);
}

float Water::WaterMap(glm::vec3 p)
{
	float frequency = water_frequency;
	float amp = wave_height;
	float chop = water_chop;

	glm::vec2 uv = glm::vec2(p.x, p.z);
	uv.x *= 0.75;

	float d = 0.0;
	float h = 0.0;

	for (int i = 0; i < water_geometry; i++)
	{
		d = WaterOctave((uv + water_time) * frequency, chop);
		d += WaterOctave((uv - water_time) * frequency, chop);
		h += d * amp;

		uv = uv * octave;
		frequency *= 1.9;
		amp *= 0.22;
		chop = glm::mix(chop, 1.0f, 0.2f);
	}

	return p.y - h;
}

float Water::WaterMapDetail(glm::vec3 p)
{
	float frequency = water_frequency;
	float amp = wave_height;
	float chop = water_chop;

	glm::vec2 uv = glm::vec2(p.x, p.z);
	uv.x *= 0.75;

	float d = 0.0;
	float h = 0.0;

	for (int i = 0; i < water_fragment; i++)
	{
		d = WaterOctave((uv + water_time) * frequency, chop);
		d += WaterOctave((uv - water_time) * frequency, chop);
		h += d * amp;

		uv = uv * octave;
		frequency *= 1.9;
		amp *= 0.22;
		chop = glm::mix(chop, 1.0f, 0.2f);
	}

	return p.y - h;
}

//Generates a height map for water simulation.
float Water::HeightMapTrace(glm::vec3 orientation, glm::vec3 direction, glm::vec3 pos)
{
	float tm = 0.0;
	float tx = 1000.0;
	float hx = WaterMap(orientation + direction * tx);

	if (hx > 0.0)
	{
		return tx;
	}

	float hm = WaterMap(orientation + direction * tm);
	float t_mid = 0.0;

	for (int i = 0; i < step_number; i++)
	{
		t_mid = glm::mix(tm, tx, hm / (hm - hx));
		pos = orientation + direction * t_mid;

		float h_mid = WaterMap(pos);

		if (h_mid < 0.0)
		{
			tx = t_mid;
			hx = h_mid;
		}

		else
		{
			tm = t_mid;
			hm = h_mid;
		}
	}
	return t_mid;
}

// Generate euler matrices.
glm::mat3 Water::GetEulerMatrices(glm::vec3 angle)
{
	glm::vec2 a_1 = glm::vec2(sin(angle.x), cos(angle.x));
	glm::vec2 a_2 = glm::vec2(sin(angle.y), cos(angle.y));
	glm::vec2 a_3 = glm::vec2(sin(angle.z), cos(angle.z));
	glm::mat3 mat;
	mat[0] = glm::vec3(a_1.y * a_3.y + a_1.x * a_2.x * a_3.x, a_1.y * a_2.x * a_3.x + a_3.y * a_1.x, -a_2.y * a_3.x);
	mat[1] = glm::vec3(-a_2.y * a_1.x, a_1.y * a_2.y, a_2.x);
	mat[2] = glm::vec3(a_3.y * a_1.x * a_2.x + a_1.y * a_3.x, a_1.x * a_3.x - a_1.y * a_3.y * a_2.x, a_2.y * a_3.y);

	return mat;
}
