#include <pwgl/Texture.hpp>

using namespace pwgl;

Texture::Texture()
{

}

Texture::~Texture()
{
	DeleteTexture();
}

void Texture::GenTexture()
{
	glGenTextures(1, &water_tex);
	glBindTexture(GL_TEXTURE_2D, water_tex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 100, 100, 0, GL_RGB, GL_FLOAT, nullptr);
	glBindTexture(GL_TEXTURE_2D, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

void Texture::DeleteTexture()
{
	glDeleteTextures(1, &water_tex);
}

void Texture::RenderTexture()
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, water_tex);
	glUniform1i(glGetUniformLocation(texture_prog, "water_texture"), 0);
}

void Texture::BindTexture()
{
	glBindTexture(GL_TEXTURE_2D, water_tex);
}
