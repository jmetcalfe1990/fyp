#include <pwgl/Mesh.hpp>
#include <pwgl/Water.hpp>

using namespace pwgl;

Mesh::Mesh()
{
	PopulateVerts();
	BuildMesh();
	CreateMesh();
}

Mesh::~Mesh()
{
	DeleteMesh();
}

int Mesh::GetVertices()
{
	return vertices.size();
}

unsigned int Mesh::GetElements()
{
	return elements.size();
}

GLuint Mesh::GetVAO()
{
	return vao;
}

void Mesh::BindVAO()
{
	glBindVertexArray(vao);
}

void Mesh::DrawElements()
{
	glDrawElements(GL_TRIANGLES, elements.size(), GL_UNSIGNED_INT, 0);
}

void Mesh::ApplyNoise()
{
	// Set number of octaves.
	// Modify frequency of perlin.
	// Modify persistance of perlin.

	water_noise.SetOctaveCount(6);
	water_noise.SetFrequency(1.0);
	water_noise.SetPersistence(0.5);

	water_mapbuilder.SetSourceModule(water_noise);
	water_mapbuilder.SetDestNoiseMap(water_heightmap);
	water_mapbuilder.SetDestSize(66, 66);
	water_mapbuilder.SetBounds(2.0, 6.0, 1.0, 5.0);
	water_mapbuilder.Build();

	int count = 0;
	for (int z = 0; z < vertex_height + 1; z++)
	{
		for (int x = 0; x < vertex_width + 1; x++)
		{
			vertices[count].position.y = water_heightmap.GetValue(x, z) * 2;
			count++;
		}
	}
	PopulateNormals();
}

// Initialise all buffer objects and vertex arrays.
void Mesh::CreateMesh()
{
	glGenBuffers(1, &vertex_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_vbo);
	glBufferData(GL_ARRAY_BUFFER, 
		vertices.size() * sizeof(Vertex), 
		nullptr, GL_STREAM_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &element_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_vbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 
		elements.size() * sizeof(unsigned int), 
		elements.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vertex_vbo);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
		sizeof(Vertex), (void*)offsetof(Vertex, position));

	glBindBuffer(GL_ARRAY_BUFFER, vertex_vbo);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
		sizeof(Vertex), (void*)offsetof(Vertex, normal));

	glBindBuffer(GL_ARRAY_BUFFER, vertex_vbo);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE,
		sizeof(Vertex), (void*)offsetof(Vertex, texCoord));

	glBindBuffer(GL_ARRAY_BUFFER, vertex_vbo);
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE,
		sizeof(Vertex), (void*)offsetof(Vertex, tangent));

	glBindBuffer(GL_ARRAY_BUFFER, vertex_vbo);
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE,
		sizeof(Vertex), (void*)offsetof(Vertex, bitangent));

	glBindBuffer(GL_ARRAY_BUFFER, vertex_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void Mesh::DeleteMesh()
{
	glDeleteBuffers(1, &vertex_vbo);
	glDeleteBuffers(1, &element_vbo);
	glDeleteVertexArrays(1, &vao);
}

// Render mesh data.
void Mesh::BuildMesh()
{
	for (int z = 0; z < vertex_height; z++)
	{
		for (int x = 0; x < vertex_width; x++)
		{
			int index = z * (vertex_height + 1) + x;

			if ((x % 2 == 0 && z % 2 == 0) || (x % 2 == 1 && z % 2 == 1))
			{
				elements.push_back(index);
				elements.push_back(index + 1);
				elements.push_back(index + (vertex_width + 2));

				elements.push_back(index);
				elements.push_back(index + (vertex_width + 2));
				elements.push_back(index + (vertex_width + 1));
			}

			else
			{
				elements.push_back(index);
				elements.push_back(index + 1);
				elements.push_back(index + (vertex_width + 1));

				elements.push_back(index + 1);
				elements.push_back(index + (vertex_width + 2));
				elements.push_back(index + (vertex_width + 1));
			}
		}
	}
	PopulateNormals();
}

// Populate vertex data.
void Mesh::PopulateVerts()
{
	for (int z = 0; z < vertex_height + 1; z++)
	{
		for (int x = 0; x < vertex_width + 1; x++)
		{
			Vertex p;
			p.position = glm::vec3(x * cell_size, 0, z * cell_size);
			vertices.push_back(p);
		}
	}
	PopulateNormals();
}

// Populate vertex normals.
void Mesh::PopulateNormals()
{
	for (auto &normals : vertices)
	{
		normals.normal = glm::vec3(0.0, 0.0, 0.0);
	}

	for (int i = 0; i < elements.size(); i += 3)
	{
		glm::vec3 pos_0 = (vertices[elements[i]].position);
		glm::vec3 pos_1 = (vertices[elements[i + 1]].position);
		glm::vec3 pos_2 = (vertices[elements[i + 2]].position);

		glm::vec3 edge_0 = pos_1 - pos_0;
		glm::vec3 edge_1 = pos_2 - pos_0;

		glm::vec3 cross = glm::cross(edge_0, edge_1);

		vertices[elements[i]].normal += cross;
		vertices[elements[i + 1]].normal += cross;
		vertices[elements[i + 2]].normal += cross;
	}

	for (auto &normals : vertices)
	{
		normals.normal = glm::normalize(normals.normal);
	}
}

// Update buffer data to sub buffer data for simulation purposes.
void Mesh::UpdateBuffer()
{
	glBindBuffer(GL_ARRAY_BUFFER, vertex_vbo);
	glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size() * sizeof(Vertex), vertices.data());
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

// Apply simulation data to vertex data for water simulation.
void Mesh::Simulate(float deltaTime)
{
	time += deltaTime;

	for (int z = 0; z < vertex_height + 1; z++)
	{
		for (int x = 0; x < vertex_width + 1; x++)
		{
			int index = z * (vertex_height + 1) + x;
			
			float x_offset = sin(vertices[index].position.x + (time / 1000));
			float z_offset = cos(vertices[index].position.z + (time / 1000));

			vertices[index].position.y = water_noise.GetValue(x_offset, (time / 10), z_offset);
		}
	}
	UpdateBuffer();
	PopulateNormals();
}
