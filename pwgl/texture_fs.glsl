#version 420 core

uniform sampler2D water_texture;

in vec3 varying_position;
in vec3 varying_normal;
in vec2 varying_texcoord;

out vec4 fragment_colour;

void main(void)
{
	fragment_colour = texture(water_tex, varying_texcoord);
}